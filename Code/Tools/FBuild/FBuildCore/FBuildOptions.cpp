// FBuild - the main application
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Tools/FBuild/FBuildCore/PrecompiledHeader.h"

// FBuildCore
#include "FBuildOptions.h"

// Core
#include "Core/Env/Env.h"
#include "Core/FileIO/PathUtils.h"
#include "Core/Math/Murmur3.h"

#include "Core\Containers\AutoPtr.h"

// Log
#include "FLog.h"

// system
#if defined( __WINDOWS__ )
    #include <windows.h> // for QueryDosDeviceA
#endif

// CONSTRUCTOR - FBuildOptions
//------------------------------------------------------------------------------
FBuildOptions::FBuildOptions()
: m_ForceCleanBuild( false )
, m_UseCacheRead( false )
, m_UseCacheWrite( false )
, m_ShowInfo( false )
, m_ShowCommandLines( false )
, m_ShowErrors( true )
, m_ShowProgress( false )
, m_ShowTcpDebug( false )
, m_AllowDistributed( false )
, m_ShowSummary( false )
, m_SaveDBOnCompletion( false )
, m_GenerateReport( false )
, m_NoLocalConsumptionOfRemoteJobs( false )
, m_AllowLocalRace( true )
, m_WrapperChild( false )
, m_FixupErrorPaths( false )
, m_StopOnFirstError( true )
, m_IgnoreRemoteFailure( false )
, m_WorkingDirHash( 0 )
{
#ifdef DEBUG
	m_ShowInfo = true; // uncomment this to enable spam when debugging
#endif

	// Default to NUMBER_OF_PROCESSORS
	m_NumWorkerThreads = Env::GetNumProcessors();

	// Default working dir is the system working dir
    AStackString<> workingDir;
    VERIFY( FileIO::GetCurrentDir( workingDir ) );
    SetWorkingDir( workingDir );
}

// FBuildOptions::SetWorkingDir
//------------------------------------------------------------------------------
void FBuildOptions::SetWorkingDir( const AString & path )
{
    ASSERT( !path.IsEmpty() );
	m_WorkingDir = path;

	// clean path
	PathUtils::FixupFolderPath( m_WorkingDir );
        
    // no trailing slash
    if ( m_WorkingDir.EndsWith( NATIVE_SLASH ) )
    {
        m_WorkingDir.SetLength( m_WorkingDir.GetLength() - 1 );
    }

	#if defined( __WINDOWS__ )
		// so C:\ and c:\ are treated the same on Windows, for better cache hits
		// make the drive letter always uppercase
		if ( ( m_WorkingDir.GetLength() >= 2 ) &&
			 ( m_WorkingDir[ 1 ] == ':' ) &&
			 ( m_WorkingDir[ 0 ] >= 'a' ) &&
			 ( m_WorkingDir[ 0 ] <= 'z' ) )
		{
			m_WorkingDir[ 0 ] = ( 'A' + ( m_WorkingDir[ 0 ] - 'a' ) );
		}
	#endif

    // Generate Mutex/SharedMemory names
    #if defined( __WINDOWS__ ) || defined( __OSX__ )
        #if defined( __WINDOWS__ )
            // convert subst drive mappings to the read path
            // (so you can't compile from the real path and the subst path at the
            // same time which would cause problems)
            AStackString<> canonicalPath;
            if ( ( m_WorkingDir.GetLength() >= 2 ) &&
                 ( m_WorkingDir[ 1 ] == ':' ) &&
                 ( m_WorkingDir[ 0 ] >= 'A' ) &&
                 ( m_WorkingDir[ 0 ] <= 'Z' ) )
            {
                // get drive letter without slash
                AStackString<> driveLetter( m_WorkingDir.Get(), m_WorkingDir.Get() + 2 );

                // get real path for drive letter (could be the same, or a subst'd path)
                char actualPath[ MAX_PATH ];
                actualPath[ 0 ] = '\000';
                VERIFY( QueryDosDeviceA( driveLetter.Get(), actualPath, MAX_PATH ) );

                // if returned path is of format "\??\C:\Folder"...
                if ( AString::StrNCmp( actualPath, "\\??\\", 4 ) == 0 )
                {
                    // .. then it means the working dir is a subst folder
                    // trim the "\\??\\" and use the real path as a base
                    canonicalPath = &actualPath[ 4 ];
                    canonicalPath += ( m_WorkingDir.Get() + 2 ); // add everything after the subst drive letter
                }
                else
                {
                    // The path was already a real path (QueryDosDevice returns the volume only)
                    canonicalPath = m_WorkingDir;
                }
            }
            else
            {
                // a UNC or other funky path - just leave it as is
                canonicalPath = m_WorkingDir;
            }
        #elif defined( __OSX__ )
            AStackString<> canonicalPath( m_WorkingDir );
        #endif

        // case insensitive
        canonicalPath.ToLower();
    #elif defined( __LINUX__ )
        // case sensitive
        AStackString<> canonicalPath( m_WorkingDir );
    #endif

    m_WorkingDirHash = Murmur3::Calc32( canonicalPath );
    m_ProcessMutexName.Format( "Global\\FASTBuild-0x%08x", m_WorkingDirHash );
    m_FinalProcessMutexName.Format( "Global\\FASTBuild_Final-0x%08x", m_WorkingDirHash );
    m_SharedMemoryName.Format( "FASTBuildSharedMemory_%08x", m_WorkingDirHash );
}

// FBuildOptions::SetConfigFile
//------------------------------------------------------------------------------
void FBuildOptions::SetConfigFile(const char * configFile)
{
	m_ConfigFile = configFile;
	if (m_ConfigFile.EndsWithI(".bff"))
	{
		m_DepGraphFile = AString(m_ConfigFile.Get(), m_ConfigFile.FindLast('.'));
		m_DepGraphFile += ".fdb";
	}
	else
	{
		m_ConfigFile = "fbuild.bff";
		m_DepGraphFile = "fbuild.fdb";
	}
}

// FBuildOptions::GetDepGraphFileName
//------------------------------------------------------------------------------
const char * FBuildOptions::GetDepGraphFileName() const
{
	return m_DepGraphFile.Get();
}

// FBuildOptions::GetBFFFileName
//------------------------------------------------------------------------------
const char * FBuildOptions::GetBFFFileName() const
{
	return m_ConfigFile.Get();
}

// FBuildOptions::InitCustomConfig
//------------------------------------------------------------------------------
void FBuildOptions::InitCustomConfig(const char * path)
{
	FLOG_INFO("InitCustomConfig: %s\n", path);
	FileStream confStream;
	if (confStream.Open(path) == false)
	{
		// missing bff is a fatal problem
		FLOG_ERROR("Failed to open Custom config file '%s'", path);
		return;
	}

	AString directory;
	PathUtils::GetFileDirectory((AString)path, directory);

	// read entire config into memory
	uint32_t size = (uint32_t)confStream.GetFileSize();
	AutoPtr< char > data((char *)ALLOC(size + 1)); // extra byte for null character sentinel	
	if (confStream.Read(data.Get(), size) != size)
	{
		FLOG_ERROR("Error reading Custom config '%s'", path);
		return;
	}

	// get all cache ignore list
	data.Get()[size] = '\0';
	AString configs(data.Get());

	Array< AString > tokens;
	configs.Tokenize(tokens, "\r\n", 2);
	int configType = 0;

	Array< AString > test;
	for (size_t i = 0; i < tokens.GetSize(); ++i)
	{
		if (tokens[i].IsEmpty())
		{
			continue;
		}
		if (tokens[i].BeginsWith("["))
		{
			configType = GetCustomConfigType(tokens[i]);
		}
		else
		{
			if (configType == 1 || configType == 2)
			{
				AStackString<> filePath(directory);
				AString currentDir(MAX_PATH);
				char fullPath[MAX_PATH] = { 0 };
				filePath += "\\";
				filePath += tokens[i];

				#if defined( __WINDOWS__ )
					::GetCurrentDirectoryA(MAX_PATH, currentDir.Get());
					::GetFullPathNameA(filePath.Get(), MAX_PATH, fullPath, NULL);				
				#elif defined( __APPLE__ ) || defined( __LINUX__ )
					Assert(false);	// Not implemented - implement if required
				#else
					#error Unknown platform
				#endif

				if (configType == 1)
				{
					m_SourceIgnoreList.Append(AString(fullPath));
					FLOG_INFO("Append source ignore file: %s\n", fullPath);
				}
				if (configType == 2)
				{
					m_IncludeIgnoreList.Append(AString(fullPath));
					FLOG_INFO("Append include ignore file: %s\n", fullPath);
				}
			}
			if (configType == 3)
			{
				m_WorkerIgnoreList.Append(tokens[i]);
				FLOG_INFO("Append worker ignore: %s\n", tokens[i].Get());
			}
		}
	}
}

// FBuildOptions::GetCustomConfigType
//------------------------------------------------------------------------------
int FBuildOptions::GetCustomConfigType(const AString & token)
{
	if (token.BeginsWith("[Source Ignore]"))
	{
		return 1;
	}
	else if (token.BeginsWith("[Include Ignore]"))
	{
		return 2;
	}
	else if (token.BeginsWith("[Worker Ignore]"))
	{
		return 3;
	}
	return 0;
}

// FBuildOptions::FindInCacheIgnore
//------------------------------------------------------------------------------
bool FBuildOptions::FindInSourceIgnore(const AString & name) const
{
	return m_SourceIgnoreList.Find(name) != nullptr;
}

bool FBuildOptions::FindInIncludeIgnore(const Array< AString > & includes) const
{	
	Array< AString >::Iter iter = m_IncludeIgnoreList.Begin();	
	for (; iter != m_IncludeIgnoreList.End(); ++iter)
	{	
		if (includes.Find(*iter, AString::EqualI))
		{
			FLOG_INFO("Find in include ignore OK: %s\n", (*iter).Get());
			return true;
		}
	}
	return false;
}

bool FBuildOptions::FindInWorkerIgnore(const AString & worker) const
{
	return m_WorkerIgnoreList.Find(worker) != nullptr;
}

//------------------------------------------------------------------------------
