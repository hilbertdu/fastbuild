// ExecNode.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Tools/FBuild/FBuildCore/PrecompiledHeader.h"

#include "ExecNode.h"
#include "Tools/FBuild/FBuildCore/WorkerPool/WorkerThread.h"

#include "Tools/FBuild/FBuildCore/FBuild.h"
#include "Tools/FBuild/FBuildCore/FLog.h"
#include "Tools/FBuild/FBuildCore/Graph/NodeGraph.h"
#include "Tools/FBuild/FBuildCore/WorkerPool/Job.h"
#include "Tools/FBuild/FBuildCore/WorkerPool/JobQueue.h"
#include "Tools/FBuild/FBuildCore/WorkerPool/JobQueueRemote.h"
#include "Tools/FBuild/FBuildCore/Helpers/Compressor.h"
#include "Tools/FBuild/FBuildCore/Helpers/ToolManifest.h"

#include "Tools/FBuild/FBuildCore/Graph/CompilerNode.h"
#include "Tools/FBuild/FBuildCore/Helpers/ToolManifest.h"

#include "Core/FileIO/FileIO.h"
#include "Core/FileIO/FileStream.h"
#include "Core/Math/Conversions.h"
#include "Core/Strings/AStackString.h"
#include "Core/Process/Process.h"
#include "Core/FileIO/PathUtils.h"
#include "Core/Math/Random.h"
#include "Core/Process/Thread.h"

// CONSTRUCTOR
//------------------------------------------------------------------------------
ExecNode::ExecNode(const Array<AString> & dstFileNames,
					    FileNode * sourceFile,
						FileNode * executable,
						const AString & arguments,
						const AString & workingDir,
						const AString & prefixDir,
						bool enableRemote,
						bool threadLock,
						int32_t expectedReturnCode,
						uint32_t retryCount,
						const Dependencies & preBuildDependencies )
: FileNode( dstFileNames[0], Node::FLAG_NONE )
, m_SourceFile( sourceFile )
, m_Executable( executable )
, m_Arguments( arguments )
, m_WorkingDir( workingDir )
, m_RetryCount( retryCount )
, m_EnableRemote( enableRemote )
, m_ThreadLock( threadLock )
, m_ExpectedReturnCode( expectedReturnCode )
, m_Remote( false )
{
	ASSERT( sourceFile );
	ASSERT( executable );	// if remote, executable used as complier node
	m_StaticDependencies.SetCapacity( 2 );
	m_StaticDependencies.Append( Dependency( executable ) );
	m_StaticDependencies.Append( Dependency( sourceFile ) );
	m_Type = EXEC_NODE;

	m_ExtraFiles = dstFileNames;
	m_ExtraFiles.PopFront();

	m_PreBuildDependencies = preBuildDependencies;
	SetPrefixDir(prefixDir);
}

// CONSTRUCTOR (remote)
//------------------------------------------------------------------------------
ExecNode::ExecNode(const Array<AString> & dstFileNames,
					FileNode * srcFile,
					const AString & arguments,
					const AString & workingDir,
					const AString & prefixDir,
                    int32_t expectedReturnCode,
					uint32_t retryCount,
					bool threadLock)
: FileNode( dstFileNames[0], Node::FLAG_NONE )
, m_SourceFile(srcFile)
, m_Arguments(arguments)
, m_WorkingDir(workingDir)
, m_RetryCount( retryCount )
, m_ThreadLock( threadLock )
, m_ExpectedReturnCode(expectedReturnCode)
, m_Remote(true)
{
	ASSERT(srcFile);
    m_Type = EXEC_NODE;
	m_LastBuildTimeMs = 5000; // higher default than a file node

	m_ExtraFiles = dstFileNames;
	m_ExtraFiles.PopFront();

	m_StaticDependencies.SetCapacity(2);
	m_StaticDependencies.Append(Dependency(nullptr));
	m_StaticDependencies.Append(Dependency(srcFile));
	SetPrefixDir(prefixDir);
}

// DESTRUCTOR
//------------------------------------------------------------------------------
ExecNode::~ExecNode()
{
	// remote worker owns the ProxyNode for the source file, so must free it
	if (m_Remote)
	{
		FDELETE m_SourceFile;
	}
}

// DoBuild
//------------------------------------------------------------------------------
/*virtual*/ Node::BuildResult ExecNode::DoBuild( Job * job )
{
	if (FBuild::Get().GetOptions().m_AllowDistributed && m_EnableRemote && 
		JobQueue::Get().GetDistributableJobsMemUsage() < (512 * MEGABYTE))
	{
		size_t dataSize = 0;
		void * data = const_cast<void *>(m_SourceFile->LoadFileLocal(dataSize));
		job->OwnData(data, dataSize);

		// compress job data
		Compressor c;
		c.Compress(job->GetData(), job->GetDataSize());
		size_t compressedSize = c.GetResultSize();
		job->OwnData(c.ReleaseResult(), compressedSize, true);

		// yes... re-queue for secondary build
		return NODE_RESULT_NEED_SECOND_BUILD_PASS;
	}
	else
	{
		if (FBuild::Get().GetOptions().m_AllowDistributed)
		{
			return NODE_RESULT_PENDING_FOR_LOCAL;
		}
		else
		{
			DoBuildPrepare(job);
			Node::BuildResult result = DoBuildProcess(job, false, false);
			DoBuildFinalize(job);
			return result;
		}
	}
}

// DoBuild2
//------------------------------------------------------------------------------
/*virtual*/ Node::BuildResult ExecNode::DoBuild2(Job * job, bool racingRemoteJob)
{
	FLOG_INFO("DoBuild2: %s\n", job->GetNode()->GetName().Get());

	DoBuildPrepare(job);
	bool stealingRemoteJob = job->IsLocal(); // are we stealing a remote job?
	Node::BuildResult result = DoBuildProcess(job, stealingRemoteJob, racingRemoteJob);
    DoBuildFinalize(job);

	FLOG_INFO("DoBuild2 end: %s\n", job->GetNode()->GetName().Get());

    return result;
}

// DoBuildPending
//------------------------------------------------------------------------------
/*virtual*/ Node::BuildResult ExecNode::DoBuildPending(Job * job)
{
	// try to reput into remote queue
	if (FBuild::Get().GetOptions().m_AllowDistributed && m_EnableRemote &&
		JobQueue::Get().GetDistributableJobsMemUsage() < (512 * MEGABYTE))
	{
		size_t dataSize = 0;
		void * data = const_cast<void *>(m_SourceFile->LoadFileLocal(dataSize));
		job->OwnData(data, dataSize);

		// compress job data
		Compressor c;
		c.Compress(job->GetData(), job->GetDataSize());
		size_t compressedSize = c.GetResultSize();
		job->OwnData(c.ReleaseResult(), compressedSize, true);

		// yes... re-queue for secondary build
		return NODE_RESULT_NEED_SECOND_BUILD_PASS;
	}
	else
	{
		DoBuildPrepare(job);
		Node::BuildResult result = DoBuildProcess(job, false, false);
		DoBuildFinalize(job);
		return result;
	}
}

Node::BuildResult ExecNode::DoBuildPrepare(Job * job)
{
	if (m_ThreadLock && job->IsLocal())
	{
		JobQueue::Get().LockJob();
	}
	else if (m_ThreadLock && !job->IsLocal())
	{
		JobQueueRemote::Get().LockJob();
	}
	return NODE_RESULT_OK;
}

Node::BuildResult ExecNode::DoBuildProcess(Job * job, bool stealingRemoteJob, bool racingRemoteJob)
{
	// If the workingDir is empty, use the current dir for the process
	AStackString<> compiler;
	AStackString<> workingDir;

    if (job->IsLocal())
    {
        compiler = GetCompiler()->GetName();
        workingDir = m_WorkingDir.IsEmpty() ? nullptr : m_WorkingDir.Get();
    }
    else
    {
        ASSERT(job->GetToolManifest());
        job->GetToolManifest()->GetRemoteFilePath(0, compiler);
        //job->GetToolManifest()->GetRemotePath(workingDir);
        GetRemoteWorkingDir(job, workingDir);

        // save input files
        void const * dataToWrite = job->GetData();
        //size_t dataToWriteSize = job->GetDataSize();

        // handle compressed data
        Compressor c; // scoped here so we can access decompression buffer
        if (job->IsDataCompressed())
        {
            c.Decompress(dataToWrite);
            dataToWrite = c.GetResult();
            //dataToWriteSize = c.GetResultSize();
        }
        m_SourceFile->SaveFileRemote((const char*)dataToWrite, workingDir);
    }

    const char * envStr = FBuild::IsValid() ? FBuild::Get().GetEnvironmentString() : nullptr;
    if ((job->IsLocal() == false) && (job->GetToolManifest()))
    {
        envStr = job->GetToolManifest()->GetRemoteEnvironmentString();
    }

	AStackString<> fullArgs(m_Arguments);
	fullArgs.Replace("%1", m_SourceFile->GetName().Get());
	fullArgs.Replace("%2", GetName().Get());

	if (!job->IsLocal())
	{
		// replace arguments
		fullArgs.Replace(m_PrefixDir.Get(), workingDir.Get());
	}

	EmitCompilationMessage(compiler, fullArgs, job->IsLocal(), stealingRemoteJob, racingRemoteJob);

	int result = -1;
	uint32_t tryCount = 0;
	Random r;
	while (tryCount++ <= m_RetryCount)
	{
		// spawn the process
		Process p;
		bool spawnOK = p.Spawn(compiler.Get(),
			fullArgs.Get(),
			workingDir.Get(),
			envStr);

		if (!spawnOK)
		{
#if defined( __WINDOWS__ )
			FLOG_ERROR("Failed to spawn process for '%s' (exe: %s) (args: %s) (%d)", GetName().Get(), compiler.Get(), fullArgs.Get(), GetLastError());
#else
			FLOG_ERROR("Failed to spawn process for '%s' (exe: %s) (args: %s)", GetName().Get(), compiler.Get(), fullArgs.Get());
#endif
			job->OnSystemError();
			return NODE_RESULT_FAILED;
		}

		// capture all of the stdout and stderr
		AutoPtr< char > memOut;
		AutoPtr< char > memErr;
		uint32_t memOutSize = 0;
		uint32_t memErrSize = 0;
		p.ReadAllData(memOut, &memOutSize, memErr, &memErrSize);

		ASSERT(!p.IsRunning());
		// Get result
		result = p.WaitForExit();

		// Handle special types of failures
		HandleSystemFailures(job, result, memOut.Get(), memErr.Get());

		// did the executable fail?
		if (result != m_ExpectedReturnCode)
		{
			// something went wrong, print details
			AString retryError;
			retryError.Format("\n[Error] %s (%d)\n", job->GetNode()->GetName().Get(), tryCount);
			Node::DumpOutput(job, retryError.Get(), retryError.GetLength());
			Node::DumpOutput(job, memOut.Get(), memOutSize);
			Node::DumpOutput(job, memErr.Get(), memErrSize);

			FLOG_ERROR("Execution failed (error %i) '%s' (%s)", result, GetName().Get(), fullArgs.Get());
			//return NODE_RESULT_FAILED;
			Thread::Sleep(r.GetRandIndexRange(1000, 2000));
		}
		else
		{
			break;
		}
	}

	if (result != m_ExpectedReturnCode)
	{
		return NODE_RESULT_FAILED;
	}

	// update the file's "last modified" time
	//m_Stamp = FileIO::GetFileLastWriteTime(m_Name);
	m_Stamp = GetLastWriteTime();
	return NODE_RESULT_OK;
}

Node::BuildResult ExecNode::DoBuildFinalize(Job * job)
{
	// free lock
	if (m_ThreadLock && job->IsLocal())
	{
		JobQueue::Get().UnLockJob();
	}
	else if (m_ThreadLock && !job->IsLocal())
	{
		JobQueueRemote::Get().UnLockJob();
	}

    // clean up remote files
	if (!job->IsLocal())
	{
		m_SourceFile->DeleteAllFile();
	}
    return NODE_RESULT_OK;
}

// Load
//------------------------------------------------------------------------------
/*static*/ Node * ExecNode::Load( IOStream & stream )
{
	NODE_LOAD( Array<AString>,	fileNames );
	NODE_LOAD( AStackString<>,	sourceFile );
	NODE_LOAD( AStackString<>,	executable );
	NODE_LOAD( AStackString<>,	arguments );
	NODE_LOAD( AStackString<>,	workingDir );
	NODE_LOAD( AStackString<>,	prefixDir );
	NODE_LOAD( bool, enableRemote );
	NODE_LOAD( bool, threadLock );
	NODE_LOAD( int32_t,			expectedReturnCode );
	NODE_LOAD( int32_t,			retryCount);
	NODE_LOAD_DEPS( 0,			preBuildDependencies );

	NodeGraph & ng = FBuild::Get().GetDependencyGraph();
	Node * srcNode = ng.FindNode( sourceFile );
	ASSERT( srcNode ); // load/save logic should ensure the src was saved first
	ASSERT( srcNode->IsAFile() );
	Node * execNode = ng.FindNode( executable );
	ASSERT( execNode ); // load/save logic should ensure the src was saved first
	ASSERT( execNode->IsAFile() );
	ExecNode * n = ng.CreateExecNode( fileNames,
								  (FileNode *)srcNode,
								  (FileNode *)execNode,
								  arguments,
								  workingDir,
								  prefixDir,
								  enableRemote,
								  threadLock,
								  expectedReturnCode,
								  retryCount,
								  preBuildDependencies );
	ASSERT( n );

	return n;
}

// Save
//------------------------------------------------------------------------------
/*virtual*/ void ExecNode::Save( IOStream & stream ) const
{
	NODE_SAVE( m_Name );
	NODE_SAVE( m_SourceFile->GetName() );
	NODE_SAVE( m_Executable->GetName() );
	NODE_SAVE( m_Arguments );
	NODE_SAVE( m_WorkingDir );
	NODE_SAVE( m_PrefixDir );
    NODE_SAVE( m_EnableRemote );
	NODE_SAVE( m_ThreadLock );
	NODE_SAVE( m_ExpectedReturnCode );
	NODE_SAVE( m_RetryCount );
	NODE_SAVE_DEPS( m_PreBuildDependencies );
}

/*virtual*/ void ExecNode::SaveRemote(IOStream & stream) const
{
	// Save minimal information for the remote worker
	Array<AString> files(m_ExtraFiles.GetSize() + 1);
	files.Append(m_Name);
	files.Append(m_ExtraFiles);

	NODE_SAVE(files);
	NODE_SAVE(m_Arguments);
	NODE_SAVE(m_WorkingDir);
	NODE_SAVE(m_PrefixDir);
    NODE_SAVE(m_ExpectedReturnCode);
	NODE_SAVE(m_RetryCount);
	NODE_SAVE(m_ThreadLock);

	m_SourceFile->SaveRemote(stream);
}

/*static*/ Node * ExecNode::LoadRemote(IOStream & stream)
{
	NODE_LOAD(Array<AString>, files);
	NODE_LOAD(AStackString<>, arguments);
	NODE_LOAD(AStackString<>, workingdir);
	NODE_LOAD(AStackString<>, prefixDir);
    NODE_LOAD(int32_t, expectedReturnCode);
	NODE_LOAD(uint32_t, retryCount);
	NODE_LOAD(bool, threadLock);

	FileNode* srcFile = static_cast<FileNode*>(FileNode::LoadRemote(stream));
	srcFile->SetPrefixDir(prefixDir);

	return FNEW(ExecNode(files, srcFile, arguments, workingdir, prefixDir, expectedReturnCode, retryCount, threadLock));
}

void ExecNode::GetRemoteWorkingDir(Job * job, AString & remoteWorkingDir)
{
    ASSERT(job->GetToolManifest());
    job->GetToolManifest()->GetRemotePath(remoteWorkingDir);
}

/*virtual*/ void ExecNode::ReplaceFileNames(Job * job)
{
	FileNode::ReplaceFileNames(job);

	// replace source file names
	m_SourceFile->ReplaceFileNames(job);
}

// EmitCompilationMessage
//------------------------------------------------------------------------------
void ExecNode::EmitCompilationMessage(const AString & compiler, 
                                        const AString & args, 
                                        bool localJob, 
                                        bool stealingRemoteJob, 
                                        bool racingRemoteJob) const
{
	// basic info
	AStackString< 2048 > output;
	output += "Run: ";
	output += GetName();

	if (racingRemoteJob)
	{
		output += " <LOCAL RACE>";
	}
	else if (stealingRemoteJob || localJob)
	{
		output += " <LOCAL>";
	}
	output += '\n';

	// verbose mode
    if (localJob && (FLog::ShowInfo() || FBuild::Get().GetOptions().m_ShowCommandLines))
	{
		AStackString< 1024 > verboseOutput;
		verboseOutput.Format( "%s %s\nWorkingDir: %s\nExpectedReturnCode: %i\n", 
                              compiler.Get(),
							  args.Get(),
							  m_WorkingDir.Get(),
							  m_ExpectedReturnCode );
		output += verboseOutput;
	}

	// output all at once for contiguousness
    FLOG_BUILD_DIRECT( output.Get() );
}

// HandleSystemFailures
//------------------------------------------------------------------------------
/*static*/ void ExecNode::HandleSystemFailures(Job * job, int result, const char * stdOut, const char * stdErr)
{
	// Only remote compilation has special cases. We don't touch local failures.
	if (job->IsLocal())
	{
		return;
	}

	// Nothing to check if everything is ok
	if (result == 0)
	{
		return;
	}

#if defined( __WINDOWS__ )
	// If remote PC is shutdown by user, compiler can be terminated
	if (result == DBG_TERMINATE_PROCESS)
	{
		job->OnSystemError(); // task will be retried on another worker
		return;
	}

	if (result == STATUS_DLL_NOT_FOUND)
	{
		job->OnSystemError();
		job->Error("Remote failure: STATUS_DLL_NOT_FOUND (0xC0000135L) - Please update to .net framework 4.0!\n");
		return;
	}

	if (result == STATUS_DLL_INIT_FAILED)
	{
		job->OnSystemError();
		job->Error("Remote failure: STATUS_DLL_NOT_FOUND (0xC0000142L) !\n");
		return;
	}

	// If DLLs are not correctly sync'd, add an extra message to help the user
	if (result == 0xC000007B) // STATUS_INVALID_IMAGE_FORMAT
	{
		job->Error("Remote failure: STATUS_INVALID_IMAGE_FORMAT (0xC000007B) - Check Compiler() settings!\n");
		return;
	}
#else
	(void)stdOut;
	(void)stdErr;
#endif
}

//------------------------------------------------------------------------------
