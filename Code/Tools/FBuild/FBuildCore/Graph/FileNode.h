// FileNode.h - a node that tracks a single file
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_GRAPH_FILENODE_H
#define FBUILD_GRAPH_FILENODE_H

// Includes
//------------------------------------------------------------------------------
#include "Node.h"

class Job;

// FileNode
//------------------------------------------------------------------------------
class FileNode : public Node
{
public:
	explicit FileNode( const AString & fileName, uint32_t controlFlags = Node::FLAG_TRIVIAL_BUILD );
    explicit FileNode( const AString & filename, 
                       uint32_t fileSize, 
                       const Array<AString> & extraFiles,
                       const Array<uint32_t> & extraFileSize,
					   const Array<AString> & extraDirs);
	virtual ~FileNode();

	static inline Node::Type GetTypeS() { return Node::FILE_NODE; }

	virtual bool IsAFile() const { return true; }
	virtual bool HasExtraFile() const { return m_ExtraFiles.GetSize() > 0; }

	virtual void AddExtraFiles(const Array<AString> &files) { m_ExtraFiles = files; m_ExtraFileSize.SetSize(m_ExtraFiles.GetSize()); };
	virtual void AddExtraFileAndDirs(const Array<AString> &lists);
	virtual const Array<AString> & GetExtraFiles() const { return m_ExtraFiles; };
	inline void SetPrefixDir(const AString & prefixDir) { m_PrefixDir = prefixDir; }

	static Node * Load( IOStream & stream );
	virtual void Save( IOStream & stream ) const;

	virtual void SaveRemote(IOStream & stream) const override;
	static Node * LoadRemote(IOStream & stream);

	virtual void GetRemoteFileName(Job * job, AString & remoteFileName);
	virtual void GetRemoteFileName(Job * job, const AString &srcName, AString & remoteFileName);
	virtual void ReplaceFileNames(Job * job);

	const void * LoadFileLocal(size_t & dataSize, bool includeExtra = true);
	bool         SaveFileRemote(const char* mem, const AString & replaceDir, bool includeExtra = true);
    void         DeleteAllFile();

	virtual uint64_t GetLastWriteTime() const;

protected:
	virtual BuildResult DoBuild( Job * job );

	bool LoadAFile(char * mem, const AString & filename, uint32_t & dataSize);
	bool SaveAFile(const char * mem, const AString & filename, uint32_t dataSize);

	friend class Client;

	uint32_t m_FileSize;
	AString  m_PrefixDir;
	Array<AString>  m_ExtraFiles;
	Array<uint32_t> m_ExtraFileSize;
	Array<AString>	m_ExtraDirs;
};

//------------------------------------------------------------------------------
#endif // FBUILD_GRAPH_FILENODE_H
