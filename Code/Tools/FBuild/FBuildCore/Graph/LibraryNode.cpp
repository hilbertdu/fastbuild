// LibraryNode.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Tools/FBuild/FBuildCore/PrecompiledHeader.h"

#include "LibraryNode.h"
#include "DirectoryListNode.h"
#include "UnityNode.h"

#include "Tools/FBuild/FBuildCore/FBuild.h"
#include "Tools/FBuild/FBuildCore/FLog.h"
#include "Tools/FBuild/FBuildCore/Graph/CompilerNode.h"
#include "Tools/FBuild/FBuildCore/Graph/NodeGraph.h"
#include "Tools/FBuild/FBuildCore/Graph/ObjectListNode.h"
#include "Tools/FBuild/FBuildCore/Graph/ObjectNode.h"
#include "Tools/FBuild/FBuildCore/Helpers/ResponseFile.h"

// Core
#include "Core/FileIO/FileIO.h"
#include "Core/FileIO/FileStream.h"
#include "Core/FileIO/PathUtils.h"
#include "Core/Process/Process.h"
#include "Core/Strings/AStackString.h"

// CONSTRUCTOR
//------------------------------------------------------------------------------
LibraryNode::LibraryNode( const AString & libraryName,
						  const Dependencies & inputNodes,
						  CompilerNode * compiler,
						  const AString & compilerArgs,
						  const AString & compilerArgsDeoptimized,
						  const AString & compilerOutputPath,
						  const AString & compilerOutputName,
						  const AString & librarian,
						  const AString & librarianArgs,
						  uint32_t flags,
						  ObjectNode * precompiledHeader,
						  const Dependencies & compilerForceUsing,
						  const Dependencies & preBuildDependencies,
						  const Dependencies & additionalInputs,
						  bool deoptimizeWritableFiles,
						  bool deoptimizeWritableFilesWithToken,
                          CompilerNode * preprocessor,
                          const AString &preprocessorArgs,
                          const AString & projectTLogPath )
: ObjectListNode( libraryName,
                  inputNodes,
                  compiler,
                  compilerArgs,
                  compilerArgsDeoptimized,
                  compilerOutputPath,
				  compilerOutputName,
                  precompiledHeader,
                  compilerForceUsing,
                  preBuildDependencies,
                  deoptimizeWritableFiles,
                  deoptimizeWritableFilesWithToken,
                  preprocessor,
                  preprocessorArgs )
, m_AdditionalInputs( additionalInputs )
{
	m_Type = LIBRARY_NODE;
	m_LastBuildTimeMs = 10000; // TODO:C Reduce this when dynamic deps are saved

	m_LibrarianPath = librarian; // TODO:C This should be a node
	m_LibrarianArgs = librarianArgs;
	m_Flags = flags;

    m_ProjectTLogPath = projectTLogPath;

	SaveBuildState(false);
}

// DESTRUCTOR
//------------------------------------------------------------------------------
LibraryNode::~LibraryNode()
{
}

// IsAFile
//------------------------------------------------------------------------------
/*virtual*/ bool LibraryNode::IsAFile() const
{
	return true;
}

// GatherDynamicDependencies
//------------------------------------------------------------------------------
/*virtual*/ bool LibraryNode::GatherDynamicDependencies( bool forceClean )
{
    if ( ObjectListNode::GatherDynamicDependencies( forceClean ) == false )
    {
        return false; // GatherDynamicDependencies will have emited an error
    }

	// additional libs/objects
	m_DynamicDependencies.Append( m_AdditionalInputs );
    return true;
}

// DoDynamicDependencies
//------------------------------------------------------------------------------
/*virtual*/ bool LibraryNode::DoDynamicDependencies( bool forceClean )
{
	if ( GatherDynamicDependencies(forceClean) == false )
	{
		return false; // GatherDynamicDependencies will have emitted error
	}
	// Library can have no inputs ( to support for GNU )
	return true;
}

// DoBuild
//------------------------------------------------------------------------------
/*virtual*/ Node::BuildResult LibraryNode::DoBuild( Job * UNUSED( job ) )
{
	// delete library before creation (so ar.exe will not merge old symbols)
	if ( FileIO::FileExists( GetName().Get() ) )
	{
		FileIO::FileDelete( GetName().Get() );
	}

	// Format compiler args string
	AStackString< 4 * KILOBYTE > fullArgs;
	GetFullArgs( fullArgs );

	// use the exe launch dir as the working dir
	const char * workingDir = nullptr;

    const char * environment;
    if (m_EnvironmentString != nullptr)
    {
        environment = m_EnvironmentString;
    }
    else
    {
        environment = FBuild::Get().GetEnvironmentString();
    }

	EmitCompilationMessage( fullArgs );

	// use response file?
	ResponseFile rf;
	AStackString<> responseFileArgs;
    #if defined( __APPLE__ ) || defined( __LINUX__ )
        const bool useResponseFile = false; // OSX/Linux ar doesn't support response files
    #else
		const bool useResponseFile = (fullArgs.GetLength() + m_LibrarianPath.GetLength() > 32760) && (GetFlag(LIB_FLAG_LIB) || GetFlag(LIB_FLAG_AR) || GetFlag(LIB_FLAG_ORBIS_AR) || GetFlag(LIB_FLAG_GREENHILLS_AX));
    #endif
	if ( useResponseFile )
	{
		// orbis-ar.exe requires escaped slashes inside response file
		if ( GetFlag( LIB_FLAG_ORBIS_AR ) )
		{
			rf.SetEscapeSlashes();
		}

		// write args to response file
		if ( !rf.Create( fullArgs ) )
		{
			SaveCustomDepsInfo(false);
			return NODE_RESULT_FAILED; // Create will have emitted error
		}

		// override args to use response file
		responseFileArgs.Format( "@\"%s\"", rf.GetResponseFilePath().Get() );
	}

	// spawn the process
	Process p;
	bool spawnOK = p.Spawn( m_LibrarianPath.Get(),
							useResponseFile ? responseFileArgs.Get() : fullArgs.Get(),
							workingDir,
							environment );

	if ( !spawnOK )
	{
		FLOG_ERROR( "Failed to spawn process for Library creation for '%s'", GetName().Get() );
		SaveCustomDepsInfo(false);
		return NODE_RESULT_FAILED;
	}

	// capture all of the stdout and stderr
	AutoPtr< char > memOut;
	AutoPtr< char > memErr;
	uint32_t memOutSize = 0;
	uint32_t memErrSize = 0;
	p.ReadAllData( memOut, &memOutSize, memErr, &memErrSize );

	ASSERT( !p.IsRunning() );
	// Get result
	int result = p.WaitForExit();

	if ( result != 0 )
	{
		if ( memOut.Get() ) { FLOG_ERROR_DIRECT( memOut.Get() ); }
		if ( memErr.Get() ) { FLOG_ERROR_DIRECT( memErr.Get() ); }
	}

	// did the executable fail?
	if ( result != 0 )
	{
		FLOG_ERROR( "Failed to build Library (error %i) '%s'", result, GetName().Get() );
		SaveCustomDepsInfo(false);
		return NODE_RESULT_FAILED;
	}

	// record time stamp for next time
	m_Stamp = FileIO::GetFileLastWriteTime( m_Name );
	ASSERT( m_Stamp );

	// SaveCustomDepsInfo
	SaveCustomDepsInfo(true);

	return NODE_RESULT_OK;
}

// GetFullArgs
//------------------------------------------------------------------------------
void LibraryNode::GetFullArgs( AString & fullArgs ) const
{
	Array< AString > tokens( 1024, true );
	m_LibrarianArgs.Tokenize( tokens );

	const AString * const end = tokens.End();
	for ( const AString * it = tokens.Begin(); it!=end; ++it )
	{
		const AString & token = *it;
		if ( token.EndsWith( "%1" ) )
		{
			// handle /Option:%1 -> /Option:A /Option:B /Option:C
			AStackString<> pre;
			if ( token.GetLength() > 2 )
			{
				pre.Assign( token.Get(), token.GetEnd() - 2 );
			}

			// concatenate files, unquoted
			GetInputFiles( fullArgs, pre, AString::GetEmpty() );
		}
		else if ( token.EndsWith( "\"%1\"" ) )
		{
			// handle /Option:"%1" -> /Option:"A" /Option:"B" /Option:"C"
			AStackString<> pre( token.Get(), token.GetEnd() - 3 ); // 3 instead of 4 to include quote
			AStackString<> post( "\"" );

			// concatenate files, quoted
			GetInputFiles( fullArgs, pre, post );
		}
		else if ( token.EndsWith( "%2" ) )
		{
			// handle /Option:%2 -> /Option:A
			if ( token.GetLength() > 2 )
			{
				fullArgs += AStackString<>( token.Get(), token.GetEnd() - 2 );
			}
			fullArgs += m_Name;
		}
		else if ( token.EndsWith( "\"%2\"" ) )
		{
			// handle /Option:"%2" -> /Option:"A"
			AStackString<> pre( token.Get(), token.GetEnd() - 3 ); // 3 instead of 4 to include quote
			fullArgs += pre;
			fullArgs += m_Name;
			fullArgs += '"'; // post
		}
		else
		{
			fullArgs += token;
		}

		fullArgs += ' ';
	}
}

// GetReferenceFiles
//------------------------------------------------------------------------------
void LibraryNode::GetReferenceFiles(AString & fullArgs, const AString & pre, const AString & post) const
{
    GetInputFiles(fullArgs, pre, post);
}

// DetermineFlags
//------------------------------------------------------------------------------
/*static*/ uint32_t LibraryNode::DetermineFlags( const AString & librarianName )
{
	uint32_t flags = 0;
	if ( librarianName.EndsWithI("lib.exe") ||
		 librarianName.EndsWithI("lib") )
	{
		flags |= LIB_FLAG_LIB;
	}
	else if ( librarianName.EndsWithI("ar.exe") ||
		 librarianName.EndsWithI("ar") )
	{
		if ( librarianName.FindI( "orbis-ar" ) )
		{
			flags |= LIB_FLAG_ORBIS_AR;
		}
		else
		{
			flags |= LIB_FLAG_AR;
		}
	}
	else if ( librarianName.EndsWithI( "\\ax.exe" ) ||
			  librarianName.EndsWithI( "\\ax" ) )
	{
		flags |= LIB_FLAG_GREENHILLS_AX;
	}
	return flags;
}

// EmitCompilationMessage
//------------------------------------------------------------------------------
void LibraryNode::EmitCompilationMessage( const AString & fullArgs ) const
{
	AStackString<> output;
	output += "Lib: ";
	output += GetName();
	output += '\n';
	if ( FLog::ShowInfo() || FBuild::Get().GetOptions().m_ShowCommandLines )
	{
		output += m_LibrarianPath;
		output += ' ';
		output += fullArgs;
		output += '\n';
	}
    FLOG_BUILD_DIRECT( output.Get() );
}

// Load
//------------------------------------------------------------------------------
/*static*/ Node * LibraryNode::Load( IOStream & stream )
{
	NODE_LOAD( AStackString<>,	name );
	NODE_LOAD_NODE( CompilerNode,	compilerNode );
	NODE_LOAD( AStackString<>,	compilerArgs );
	NODE_LOAD( AStackString<>,	compilerArgsDeoptimized );
	NODE_LOAD( AStackString<>,	compilerOutputPath );
	NODE_LOAD( AStackString<>,  compilerOutputName );			// Add by hilbertdu 
	NODE_LOAD_DEPS( 16,			staticDeps );
	NODE_LOAD_NODE( Node,		precompiledHeader );
	NODE_LOAD( AStackString<>,	objExtensionOverride );
    NODE_LOAD( AStackString<>,	compilerOutputPrefix );
	NODE_LOAD_DEPS( 0,			compilerForceUsing );
	NODE_LOAD_DEPS( 0,			preBuildDependencies );
	NODE_LOAD( bool,			deoptimizeWritableFiles );
	NODE_LOAD( bool,			deoptimizeWritableFilesWithToken );
	NODE_LOAD_NODE( CompilerNode, preprocessorNode );
	NODE_LOAD( AStackString<>,	preprocessorArgs );

	NODE_LOAD( AStackString<>,	librarianPath );
	NODE_LOAD( AStackString<>,	librarianArgs );
	NODE_LOAD( uint32_t,		flags );
    NODE_LOAD_DEPS( 0,			additionalInputs );
    NODE_LOAD( AStackString<>,  projectTLogPath );	

	NodeGraph & ng = FBuild::Get().GetDependencyGraph();
	LibraryNode * n = ng.CreateLibraryNode( name, 
								 staticDeps, 
								 compilerNode, 
								 compilerArgs,
								 compilerArgsDeoptimized,
								 compilerOutputPath, 
								 compilerOutputName,
								 librarianPath, 
								 librarianArgs,
								 flags,
								 precompiledHeader ? precompiledHeader->CastTo< ObjectNode >() : nullptr,
								 compilerForceUsing,
								 preBuildDependencies,
								 additionalInputs,
								 deoptimizeWritableFiles,
								 deoptimizeWritableFilesWithToken,
								 preprocessorNode,
								 preprocessorArgs,
                                 projectTLogPath );
	n->m_ObjExtensionOverride = objExtensionOverride;
    n->m_CompilerOutputPrefix = compilerOutputPrefix;

	// TODO:B Need to save the dynamic deps, for better progress estimates
	// but we can't right now because we rely on the nodes we depend on 
	// being saved before us which isn't the case for dynamic deps.
	//if ( Node::LoadDepArray( fileStream, n->m_DynamicDependencies ) == false )
	//{
	//	FDELETE n;
	//	return nullptr;
	//}
	return n;
}

// Save
//------------------------------------------------------------------------------
/*virtual*/ void LibraryNode::Save( IOStream & stream ) const
{
    ObjectListNode::Save( stream );

	NODE_SAVE( m_LibrarianPath );
	NODE_SAVE( m_LibrarianArgs );
	NODE_SAVE( m_Flags );
	NODE_SAVE_DEPS( m_AdditionalInputs );
    NODE_SAVE( m_ProjectTLogPath );
}

// SaveCustomDepsInfo
//------------------------------------------------------------------------------
void LibraryNode::SaveCustomDepsInfo(bool state)
{
	if (!m_ProjectTLogPath.IsEmpty() && FileIO::DirectoryExists(m_ProjectTLogPath))
	{
		LoadTLogFile();
		UpdateTLogInfo();
		SaveTLogFile();
		SaveBuildState(state);
		FLOG_INFO("Save tlog files succeed\n");
	}
	else
	{
		FLOG_INFO("Save tlog files failed: %s\n", m_ProjectTLogPath.Get());
	}
}

// LoadTLogFileToArray
//------------------------------------------------------------------------------
void LibraryNode::LoadTLogFileToArray(const AString &file, Array< AString > &inputs, Array< Array< AString > > &deps) const
{
    Array< AString > dataArray;
    FileStream clReadLogfs;
    if (!clReadLogfs.Open(file.Get(), FileStream::READ_ONLY))
    {
        return;
    }

    FLOG_INFO("LoadTLogFileToArray start %s\n", file.Get());

    uint32_t len = (uint32_t)clReadLogfs.GetFileSize();
    WCHAR * data = FNEW_ARRAY(WCHAR[len]());
    clReadLogfs.ReadTotalUTF16(data);
    
    char * ch = FileStream::GetCharFromWChar(data);

    AString content(ch);
    content.Tokenize(dataArray, "\r\n", 2);

    for (size_t i = 0; i < dataArray.GetSize(); ++i)
    {
        if (dataArray[i].BeginsWith('^'))
        {
            inputs.Append(AString(dataArray[i].Get() + 1, dataArray[i].GetEnd()));
            deps.Append(Array< AString >());
            //FLog::Info("src: %s\n", inputs.Top().Get());
        }
        else
        {
            deps.Top().Append(dataArray[i]);
            //FLog::Info("deps top: %s\n", dataArray[i].Get());
        }
    }
    FDELETE_ARRAY data;
    FDELETE_ARRAY ch;

    FLOG_INFO("LoadTLogFileToArray finished\n");
}

// LoadTLogFile
//------------------------------------------------------------------------------
void LibraryNode::LoadTLogFile() const
{
    // input cpp list and output obj list
    AString clReadLogPath(m_ProjectTLogPath);
    AString clWriteLogPath(m_ProjectTLogPath);
    clReadLogPath += "\\cl.read.1.tlog";
    clWriteLogPath += "\\cl.write.1.tlog";

    LoadTLogFileToArray(clReadLogPath, m_ClReadInputs, m_ClReadDeps);
    LoadTLogFileToArray(clWriteLogPath, m_ClWriteInputs, m_ClWriteDeps);
    // link file don't need to read
}

// UpdateTLogInfo
//------------------------------------------------------------------------------
void LibraryNode::UpdateTLogInfo() const
{
    // Only gather additional objectList format input
    Dependencies::Iter iterObjList = m_AdditionalInputs.Begin();
    for (; iterObjList != m_AdditionalInputs.End(); ++iterObjList)
    {
        // get all ObjectList nodes, exclude those FileNode
        Node * n(iterObjList->GetNode());
        if (n->GetType() == OBJECT_LIST_NODE)
        {
            const Dependencies &objectListDeps = n->GetDynamicDependencies();
            Dependencies::Iter iterObj = objectListDeps.Begin();
            for (; iterObj != objectListDeps.End(); ++iterObj)
            {
                Node * objNode(iterObj->GetNode());
                
                if (objNode->GetType() == OBJECT_NODE)
                {
                    Array< AString > depFiles;
                    depFiles = objNode->GetReadFiles();
                    if (objNode->CastTo<ObjectNode>()->IsUsingPCH())
                    {
                        const AString &pchFile = objNode->CastTo<ObjectNode>()->GetPchName();
                        depFiles.Append(pchFile);
                    }

                    // cl read file (cpp)
                    AString srcFile(objNode->CastTo<ObjectNode>()->GetSourceFile()->GetName());
                    //PathUtils::FixupFilePath(srcFile);
                    srcFile.ToUpper();
                    FLOG_INFO("Object src file: %s %d\n", srcFile.Get(), objNode->GetStatFlag(STATS_BUILT));

                    size_t index = m_ClReadInputs.FindPos(srcFile);
                    if (index != m_ClReadInputs.GetSize())
                    {
                        if (objNode->GetStatFlag(STATS_BUILT))
                        {
                            m_ClReadDeps[index] = depFiles;
                        }
                    }
                    else
                    {
                        FLOG_INFO("Not find src index: %s\n", srcFile.Get());
                        m_ClReadInputs.Append(srcFile);
                        m_ClReadDeps.Append(depFiles);
                    }
                }
            }
        }
    }

    AString inputFiles("");
	AString wrapper("\"");
    GetReferenceFiles(inputFiles, wrapper, wrapper);
    inputFiles.ToUpper();
    inputFiles.Tokenize(m_LinkerInputs, ' ');

	for (size_t i = 0; i < m_LinkerInputs.GetSize(); ++i)
	{
		AString inputFile;
		AString::Trim(inputFile, m_LinkerInputs[i], '"');
		m_LinkerInputs[i] = inputFile;
	}

    m_LinkerInputs.Sort(AString::LT);
    FLOG_INFO("Link input files:\n %s\n", inputFiles.Get());

    m_LinkerOutputs.Append(m_Name);
    FLOG_INFO("Link output file: %s\n", m_LinkerOutputs[0].Get());
}

// SaveTLogFile
//------------------------------------------------------------------------------
void LibraryNode::SaveTLogFile() const
{
    if (m_ProjectTLogPath.IsEmpty())
    {
        return;
    }

    // cl read and write file

    AString clReadLogPath(m_ProjectTLogPath);
    AString clWriteLogPath(m_ProjectTLogPath);
    clReadLogPath += "\\cl.read.1.tlog";
    clWriteLogPath += "\\cl.write.1.tlog";

    FileStream clReadLogfs;
    clReadLogfs.Open(clReadLogPath.Get(), FileStream::WRITE_ONLY);
    clReadLogfs.WriteUTF16Head();

    for (size_t i = 0; i < m_ClReadInputs.GetSize(); ++i)
    {
        wchar_t *inputs = FileStream::GetWCharFromChar(m_ClReadInputs[i].Get());
        clReadLogfs.WriteUTF16(L"^", 2);
        clReadLogfs.WriteUTF16(inputs, m_ClReadInputs[i].GetLength() * 2);
        clReadLogfs.WriteUTF16(L"\r\n", 4);
        FDELETE_ARRAY inputs;

        for (size_t j = 0; j < m_ClReadDeps[i].GetSize(); ++j)
        {
			if (m_ClReadDeps[i][j].IsEmpty() || m_ClReadDeps[i][j] == "\n" || m_ClReadDeps[i][j] == "\r\n")
			{
				continue;
			}
            wchar_t *dep = FileStream::GetWCharFromChar(m_ClReadDeps[i][j].Get());            
			//FLog::Info("Include : %s -- %x -- %d\n", m_ClReadDeps[i][j].Get(), m_ClReadDeps[i][j].Get(), m_ClReadDeps[i][j].GetLength());
            clReadLogfs.WriteUTF16(dep, m_ClReadDeps[i][j].GetLength() * 2);
            clReadLogfs.WriteUTF16(L"\r\n", 4);
            FDELETE_ARRAY dep;
        }
    }
    clReadLogfs.Close();

    // linker read and write file

    AString linkReadLogPath(m_ProjectTLogPath);
    AString linkWriteLogPath(m_ProjectTLogPath);
    linkReadLogPath += "\\lib-link.read.1.tlog";
    linkWriteLogPath += "\\lib-link.write.1.tlog";

    FileStream linkReadLogfs;
    linkReadLogfs.Open(linkReadLogPath.Get(), FileStream::WRITE_ONLY);
    linkReadLogfs.WriteUTF16Head();

    FileStream linkWriteLogfs;
    linkWriteLogfs.Open(linkWriteLogPath.Get(), FileStream::WRITE_ONLY);
    linkWriteLogfs.WriteUTF16Head();

    linkReadLogfs.WriteUTF16(L"^", 2);
    linkWriteLogfs.WriteUTF16(L"^", 2);
    for (size_t i = 0; i < m_LinkerInputs.GetSize(); ++i)
    {
		PathUtils::FixupFilePath(m_LinkerInputs[i]);
        wchar_t *input = FileStream::GetWCharFromChar(m_LinkerInputs[i].Get());
        linkReadLogfs.WriteUTF16(input, m_LinkerInputs[i].GetLength() * 2);
        linkWriteLogfs.WriteUTF16(input, m_LinkerInputs[i].GetLength() * 2);

        if (i != m_LinkerInputs.GetSize() - 1)
        {
            linkReadLogfs.WriteUTF16(L"|", 2);
            linkWriteLogfs.WriteUTF16(L"|", 2);
        }
        FDELETE_ARRAY input;
    }

    for (size_t i = 0; i < m_LinkerInputs.GetSize(); ++i)
    {
        wchar_t *input = FileStream::GetWCharFromChar(m_LinkerInputs[i].Get());
        linkReadLogfs.WriteUTF16(L"\r\n", 4);
        linkReadLogfs.WriteUTF16(input, m_LinkerInputs[i].GetLength() * 2);
        FDELETE_ARRAY input;
    }

    for (size_t i = 0; i < m_LinkerOutputs.GetSize(); ++i)
    {
        wchar_t *output = FileStream::GetWCharFromChar(m_LinkerOutputs[i].Get());
        linkWriteLogfs.WriteUTF16(L"\r\n", 4);
        linkWriteLogfs.WriteUTF16(output, m_LinkerOutputs[i].GetLength() * 2);
        FDELETE_ARRAY output;
    }
    linkReadLogfs.Close();
    linkWriteLogfs.Close();

	FLOG_INFO("Save tlog file finished\n");
}

// SaveBuildState
//------------------------------------------------------------------------------
void LibraryNode::SaveBuildState(bool state) const
{
	AString buildStateFile(m_ProjectTLogPath);
    FLOG_INFO("Build state %s --> %d\n", GetName().Get(), state);
	if (state)
	{		
		buildStateFile += "\\unsuccessfulbuild";
		FileIO::FileDelete(buildStateFile.Get());
	}
	else
	{
		buildStateFile += "\\unsuccessfulbuild";
		FileStream buildStatefs;
		buildStatefs.Open(buildStateFile.Get(), FileStream::WRITE_ONLY);
		buildStatefs.Close();
	}
}

//------------------------------------------------------------------------------
