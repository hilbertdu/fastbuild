// FileNode.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Tools/FBuild/FBuildCore/PrecompiledHeader.h"

#include "FileNode.h"
#include "Tools/FBuild/FBuildCore/FBuild.h"
#include "Tools/FBuild/FBuildCore/FLog.h"
#include "Tools/FBuild/FBuildCore/Graph/NodeGraph.h"

#include "Tools/FBuild/FBuildCore/WorkerPool/Job.h"
#include "Tools/FBuild/FBuildCore/Helpers/ToolManifest.h"
#include "Tools/FBuild/FBuildCore/WorkerPool/WorkerThread.h"

#include "Core/FileIO/FileIO.h"
#include "Core/FileIO/FileStream.h"
#include "Core/Strings/AStackString.h"
#include "Core/Containers/AutoPtr.h"
#include "Core/FileIO/PathUtils.h"

// CONSTRUCTOR
//------------------------------------------------------------------------------
FileNode::FileNode( const AString & fileName, uint32_t controlFlags )
: Node( fileName, Node::FILE_NODE, controlFlags )
{
	ASSERT( fileName.EndsWith( "\\" ) == false );
	ASSERT( ( fileName.FindLast( ':' ) == nullptr ) ||
			( fileName.FindLast( ':' ) == ( fileName.Get() + 1 ) ) );

	m_LastBuildTimeMs = 1; // very little work required
}

FileNode::FileNode( const AString & filename,
                    uint32_t fileSize,
                    const Array<AString> & extraFiles,
                    const Array<uint32_t> & extraFileSize,
					const Array<AString> & extraDirs )
: Node( filename, Node::FILE_NODE, Node::FLAG_NONE )
, m_FileSize(fileSize)
, m_ExtraFiles(extraFiles)
, m_ExtraFileSize(extraFileSize)
, m_ExtraDirs(extraDirs)
{
    ASSERT(fileSize);
    m_Type = EXEC_NODE;
    m_LastBuildTimeMs = 5000; // higher default than a file node
}

// DESTRUCTOR
//------------------------------------------------------------------------------
FileNode::~FileNode()
{
}

// DoBuild
//------------------------------------------------------------------------------
/*virtual*/ Node::BuildResult FileNode::DoBuild( Job * job )
{
	//m_Stamp = FileIO::GetFileLastWriteTime( m_Name );
	m_Stamp = GetLastWriteTime();
	if (!job->IsLocal())
	{
		// TODO: delete tmp file
	}
	return NODE_RESULT_OK;
}

// Load
//------------------------------------------------------------------------------
/*static*/ Node * FileNode::Load( IOStream & stream )
{
	NODE_LOAD( AStackString<>,	fileName );

	NodeGraph & ng = FBuild::Get().GetDependencyGraph();
	Node * n = ng.CreateFileNode( fileName );
	ASSERT( n );
	return n;
}

// Save
//------------------------------------------------------------------------------
/*virtual*/ void FileNode::Save( IOStream & stream ) const
{
	NODE_SAVE( m_Name );
}

/*virtual*/ void FileNode::SaveRemote(IOStream & stream) const
{
	NODE_SAVE(m_Name);
    NODE_SAVE(m_FileSize);
	NODE_SAVE(m_ExtraFiles);
	NODE_SAVE(m_ExtraFileSize);
	NODE_SAVE(m_ExtraDirs);
	//FLOG_BUILD("Save remote[%s]: %d (%d, %d)\n", m_Name, m_FileSize, m_ExtraFiles.GetSize(), m_ExtraFileSize.GetSize());

	// rename file path for remote

}

/*static*/ Node * FileNode::LoadRemote(IOStream & stream)
{
	NODE_LOAD(AStackString<>, name);
    NODE_LOAD(uint32_t, fileSize);
    NODE_LOAD(Array<AString>, extraFiles);
    NODE_LOAD(Array<uint32_t>, extraFileSize);
	NODE_LOAD(Array<AString>, extraDirs);

    return FNEW(FileNode(name, fileSize, extraFiles, extraFileSize, extraDirs));
}

/*virtual*/ void FileNode::GetRemoteFileName(Job * job, AString & remoteFileName)
{
	ASSERT(job->GetToolManifest());
	GetRemoteFileName(job, m_Name, remoteFileName);
	//FLOG_BUILD("Remote output file path: %s\n", remoteFileName);
}

/*virtual*/ void FileNode::GetRemoteFileName(Job * job, const AString &srcName, AString & remoteFileName)
{
	if (m_PrefixDir.IsEmpty())
	{
		// file name should be the same as on host
		const char * baseName = (srcName.FindLast(NATIVE_SLASH) + 1);
		WorkerThread::CreateTempFilePath(baseName, remoteFileName);
	}
	else
	{
		// put file under toolchain directory
		AStackString<> remoteRelFilePath;
		AStackString<> remoteRootDir;
		job->GetToolManifest()->GetRemotePath(remoteRootDir);
		PathUtils::GetRelativePath(srcName, m_PrefixDir, remoteRelFilePath);

		remoteFileName = remoteRootDir;
		remoteFileName += remoteRelFilePath;
	}
}

/*virtual*/ void FileNode::ReplaceFileNames(Job * job)
{
	// file name should be the same as on host
	const char * fileName = (m_Name.FindLast(NATIVE_SLASH) + 1);

	// replace tmp file name
	AStackString<> tmpFileName;
	GetRemoteFileName(job, tmpFileName);
	ReplaceDummyName(tmpFileName);

	// replace extra file names
	for (size_t idx = 0; idx < m_ExtraFiles.GetSize(); ++idx)
	{
		AStackString<> replacedName;
		GetRemoteFileName(job, m_ExtraFiles[idx], replacedName);
		m_ExtraFiles[idx] = replacedName;
	}

	// replace extra dir names
	for (size_t idx = 0; idx < m_ExtraDirs.GetSize(); ++idx)
	{
		AStackString<> replacedName;
		GetRemoteFileName(job, m_ExtraDirs[idx], replacedName);
		m_ExtraDirs[idx] = replacedName;
	}
}

const void * FileNode::LoadFileLocal(size_t & dataSize, bool includeExtra)
{
	// load file data on local

	// get total size
	uint32_t totalSize = 0;
	FileStream fs;
	if (fs.Open(m_Name.Get(), FileStream::READ_ONLY) == false)
	{
		FLOG_ERROR("Error: opening file '%s' in Compiler ToolManifest\n", m_Name.Get());
		return nullptr;
	}
	m_FileSize = (uint32_t)fs.GetFileSize();
	totalSize += m_FileSize;

	if (includeExtra)
	{
		for (size_t idx = 0; idx < m_ExtraFiles.GetSize(); ++idx)
		{
			FileStream fs_ext;
			if (fs_ext.Open(m_ExtraFiles[idx].Get(), FileStream::READ_ONLY) == false)
			{
				FLOG_ERROR("Error: opening file '%s' in Compiler ToolManifest\n", m_Name.Get());
				return nullptr;
			}
			m_ExtraFileSize[idx] = (uint32_t)fs_ext.GetFileSize();
			totalSize += m_ExtraFileSize[idx];
			//FLOG_BUILD("Load file local: %s\n", m_ExtraFiles[idx].Get());
		}
	}
	dataSize = totalSize;

	// alloc memory
	void * mem = (void *)ALLOC(totalSize);

	LoadAFile((char *)mem, m_Name, m_FileSize);

	if (includeExtra)
	{
		size_t curPos = m_FileSize;
		for (size_t idx = 0; idx < m_ExtraFiles.GetSize(); ++idx)
		{
			LoadAFile((char *)mem + curPos, m_ExtraFiles[idx], m_ExtraFileSize[idx]);
			curPos += m_ExtraFileSize[idx];
		}
	}

    //FLOG_BUILD("Load file local size: %d\n", dataSize);
	return mem;
}

bool FileNode::LoadAFile(char * mem, const AString & filename, uint32_t & dataSize)
{
	// read the file into memory
	FileStream fs;
	if (fs.Open(filename.Get(), FileStream::READ_ONLY) == false)
	{
		FLOG_ERROR("Error: opening file '%s' in FileNode\n", filename.Get());
		return false;
	}
	//uint32_t contentSize = (uint32_t)fs.GetFileSize();
	//AutoPtr< void > mem(ALLOC(contentSize));
	if (fs.Read(mem, dataSize) != dataSize)
	{
		FLOG_ERROR("Error: reading file '%s' in FileNode\n", filename.Get());
		return false;
	}

    //FLOG_BUILD("Load a file: %s, %d\n", filename.Get(), dataSize);
	return true;
}

bool FileNode::SaveAFile(const char * mem, const AString & filename, uint32_t dataSize)
{
	// write to disk
	FileStream fs;
	if (!fs.Open(filename.Get(), FileStream::WRITE_ONLY))
	{
		return false; // FAILED
	}
	if (fs.Write(mem, dataSize) != dataSize)
	{
		return false; // FAILED
	}
	fs.Close();
	return true;
}

bool FileNode::SaveFileRemote(const char* mem, const AString & replaceDir, bool includeExtra)
{
	// prepare destination
	AStackString<> pathOnly(m_Name.Get(), m_Name.FindLast(NATIVE_SLASH));
	if (!FileIO::EnsurePathExists(pathOnly))
	{
		return false; // FAILED
	}

	// write to disk
	SaveAFile(mem, m_Name, m_FileSize);

	if (includeExtra)
	{
		size_t curPos = m_FileSize;
		for (size_t idx = 0; idx < m_ExtraFiles.GetSize(); ++idx)
		{
			AStackString<> pathOnly(m_ExtraFiles[idx].Get(), m_ExtraFiles[idx].FindLast(NATIVE_SLASH));
			if (!FileIO::EnsurePathExists(pathOnly))
			{
				return false; // FAILED
			}

			SaveAFile(mem + curPos, m_ExtraFiles[idx], m_ExtraFileSize[idx]);
			curPos += m_ExtraFileSize[idx];
		}
	}

	return true;
}

void FileNode::DeleteAllFile()
{
	FileIO::FileDelete(m_Name.Get());
    for (size_t idx = 0; idx < m_ExtraFiles.GetSize(); ++idx)
    {
		FileIO::FileDelete(m_ExtraFiles[idx].Get());
    }
	for (size_t idx = 0; idx < m_ExtraDirs.GetSize(); ++idx)
	{
		FileIO::DirectoryDelete(m_ExtraDirs[idx]);
	}
}

/*virtual*/ void FileNode::AddExtraFileAndDirs(const Array<AString> &lists)
{
	for (size_t idx = 0; idx < lists.GetSize(); ++idx)
	{
		if (FileIO::DirectoryExists(lists[idx]))
		{
			Array<AString> files;
			FileIO::GetFiles(lists[idx], AString("*"), true, &files);
			m_ExtraFiles.Append(files);
			FLOG_INFO("Append dir files: %d\n", files.GetSize());
			m_ExtraDirs.Append(lists[idx]);
		}
		else if (FileIO::FileExists(lists[idx].Get()))
		{
			FLOG_INFO("Append file: %s\n", lists[idx].Get());
			m_ExtraFiles.Append(lists[idx]);
		}
	}
	m_ExtraFileSize.SetSize(m_ExtraFiles.GetSize());
}

/*virtual*/ uint64_t FileNode::GetLastWriteTime() const
{
	uint64_t LastWriteTime = FileIO::GetFileLastWriteTime(m_Name);
	for (size_t idx = 0; idx < m_ExtraFiles.GetSize(); ++idx)
	{
		uint64_t time = FileIO::GetFileLastWriteTime(m_ExtraFiles[idx]);
		if (time > LastWriteTime)
		{
			LastWriteTime = time;
		}
	}
	return LastWriteTime;
}

//------------------------------------------------------------------------------
