// ExecNode
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_GRAPH_EXECNODE_H
#define FBUILD_GRAPH_EXECNODE_H

// Includes
//------------------------------------------------------------------------------
#include "FileNode.h"
#include "Core/Containers/Array.h"

// Forward Declarations
//------------------------------------------------------------------------------
class NodeProxy;

// ExecNode
//------------------------------------------------------------------------------
class ExecNode : public FileNode
{
public:
	explicit ExecNode( const Array<AString> & dstFileNames,
					    FileNode * sourceFile,
						FileNode * executable,
						const AString & arguments,
						const AString & workingDir,
						const AString & prefixDir,
						bool enableRemote,
						bool threadLock,
						int32_t expectedReturnCode,
						uint32_t retryCount,
						const Dependencies & preBuildDependencies );
	// simplified remote constructor
	explicit ExecNode(const Array<AString> & dstFileNames,
						FileNode * srcFile,
						const AString & arguments,
						const AString & workingDir,
						const AString & prefixDir,
                        int32_t expectedReturnCode,
						uint32_t retryCount,
						bool threadLock);
	virtual ~ExecNode();

	static inline Node::Type GetTypeS() { return Node::EXEC_NODE; }

	inline Node * GetCompiler() const { return m_StaticDependencies[0].GetNode(); }

	static Node * Load( IOStream & stream );
	virtual void Save( IOStream & stream ) const;

	virtual void SaveRemote(IOStream & stream) const override;
	static Node * LoadRemote(IOStream & stream);

    void GetRemoteWorkingDir(Job * job, AString & remoteWorkingDir);

	virtual void ReplaceFileNames(Job * job);

	static void HandleSystemFailures(Job * job, int result, const char * stdOut, const char * stdErr);

private:
	virtual BuildResult DoBuild( Job * job );
	virtual BuildResult DoBuild2(Job * job, bool racingRemoteJob) override;
	virtual BuildResult DoBuildPending(Job * job) override;

	BuildResult DoBuildPrepare(Job * job);
	BuildResult DoBuildProcess(Job * job, bool stealingRemoteJob, bool racingRemoteJob);
    BuildResult DoBuildFinalize(Job * job);

    void EmitCompilationMessage(const AString & compiler, const AString & args, bool localJob, bool stealingRemoteJob, bool racingRemoteJob) const;

	FileNode *	m_SourceFile;
	FileNode *	m_Executable;
	AString		m_Arguments;
	AString		m_WorkingDir;
	int32_t		m_ExpectedReturnCode;
	uint32_t	m_RetryCount;
	bool		m_EnableRemote;
	bool		m_ThreadLock;
	bool		m_Remote;
};

//------------------------------------------------------------------------------
#endif // FBUILD_GRAPH_EXECNODE_H
