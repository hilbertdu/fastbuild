// WorkerBrokerage - Manage worker discovery
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_WORKERBROKERAGE_H
#define FBUILD_WORKERBROKERAGE_H

// Includes
//------------------------------------------------------------------------------
#include "Core/Strings/AString.h"
#include "Core/Time/Timer.h"

// Forward Declarations
//------------------------------------------------------------------------------

// WorkerBrokerage
//------------------------------------------------------------------------------
class WorkerBrokerage
{
public:
	WorkerBrokerage();
	~WorkerBrokerage();

	// client interface
	void FindWorkers( Array< AString > & workerList );

	// server interface
	void SetAvailability( bool available );

	// get last worker timestamp
	void GetUpdatedWorkerPath(AStackString<> & filePath);
private:
	AString				m_Root;
	AString				m_BrokerageRoot;
	bool				m_Availability;
    AString             m_HostName;
    AString             m_BrokerageFilePath;
    Timer               m_TimerLastUpdate;      // Throttle network access
	uint32_t			m_HostIP;
};

//------------------------------------------------------------------------------
#endif // FBUILD_WORKERBROKERAGE_H 
