// FunctionTest
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Tools/FBuild/FBuildCore/PrecompiledHeader.h"

#include "FunctionExec.h"
#include "Tools/FBuild/FBuildCore/FBuild.h"
#include "Tools/FBuild/FBuildCore/BFF/BFFVariable.h"
#include "Tools/FBuild/FBuildCore/Graph/NodeGraph.h"
#include "Tools/FBuild/FBuildCore/Graph/ExecNode.h"

#include "Tools/FBuild/FBuildCore/Graph/AliasNode.h"
#include "Tools/FBuild/FBuildCore/Graph/CompilerNode.h"

// CONSTRUCTOR
//------------------------------------------------------------------------------
FunctionExec::FunctionExec()
: Function( "Exec" )
{
}

// AcceptsHeader
//------------------------------------------------------------------------------
/*virtual*/ bool FunctionExec::AcceptsHeader() const
{
	return true;
}

// Commit
//------------------------------------------------------------------------------
/*virtual*/ bool FunctionExec::Commit( const BFFIterator & funcStartIter ) const
{
	// make sure all required variables are defined
	//const BFFVariable * outputV;
	const BFFVariable * executableV;
	//const BFFVariable * inputV;
	const BFFVariable * argsV;
	const BFFVariable * workingDirV;
	const BFFVariable * prefixDirV;
	Array<AString> inputs;
	Array<AString> outputs;
	bool enableRemote = false;
	bool threadLock = false;
	int32_t expectedReturnCode;
	int32_t retryCount;
	if ( !GetStrings( funcStartIter, outputs,		".ExecOutput", true ) ||
		 !GetString( funcStartIter, executableV,	".ExecExecutable", true ) ||
		 //!GetString( funcStartIter, inputV,			".ExecInput", true ) ||
		 !GetStrings(funcStartIter, inputs,			".ExecInput", true) ||
		 !GetString( funcStartIter, argsV,			".ExecArguments" ) ||
		 !GetString( funcStartIter, workingDirV,	".ExecWorkingDir" ) ||
		 !GetString( funcStartIter, prefixDirV,		".ExecPrefixDir") ||
		 !GetBool( funcStartIter, enableRemote,		".ExecEnableRemote", false) ||
		 !GetBool( funcStartIter, threadLock,		".ExecThreadLock", false) ||
		 !GetInt( funcStartIter, expectedReturnCode, ".ExecReturnCode", 0, false ) ||
		 !GetInt( funcStartIter, retryCount,		".ExecRetryCount", 0, false))
	{
		return false;
	}

	// check for duplicates
	NodeGraph & ng = FBuild::Get().GetDependencyGraph();
	for (Array<AString>::ConstIter iter = outputs.Begin(); iter != outputs.End(); ++iter)
	{
		if (ng.FindNode(*iter) != nullptr)
		{
			Error::Error_1100_AlreadyDefined(funcStartIter, this, *iter);
			return false;
		}
	}

	// Pre-build dependencies
	Dependencies preBuildDependencies;
	if ( !GetNodeList( funcStartIter, ".PreBuildDependencies", preBuildDependencies, false ) )
	{
		return false; // GetNodeList will have emitted an error
	}

	/*// get executable node
	Node * exeNode = ng.FindNode( executableV->GetString() );
	if ( exeNode == nullptr )
	{
	exeNode = ng.CreateFileNode( executableV->GetString() );
	}
	else if ( exeNode->IsAFile() == false )
	{
	Error::Error_1103_NotAFile( funcStartIter, this, "ExecExecutable", exeNode->GetName(), exeNode->GetType() );
	return false;
	}*/

	// get compiler node
	// find or create the compiler node
	CompilerNode * compilerNode = nullptr;
	if (!FunctionExec::GetCompilerNode(funcStartIter, executableV->GetString(), compilerNode))
	{
		return false; // GetCompilerNode will have emitted error
	}

	// source node
	Node * inputNode = ng.FindNode( inputs[0] );
	if ( inputNode == nullptr )
	{
		inputNode = ng.CreateFileNode( inputs[0] );
		if (inputs.GetSize() > 1)
		{
			inputs.PopFront();
			inputNode->CastTo<FileNode>()->AddExtraFileAndDirs(inputs);
		}
	}
	else if ( inputNode->IsAFile() == false )
	{
		Error::Error_1103_NotAFile(funcStartIter, this, "ExecInput", compilerNode->GetName(), compilerNode->GetType());
		return false;
	}

	// optional args
	const AString & arguments(	argsV ?			argsV->GetString()		: AString::GetEmpty() );
	const AString & workingDir( workingDirV ?	workingDirV->GetString(): AString::GetEmpty() );
	const AString & prefixDir( prefixDirV ? prefixDirV->GetString() : AString::GetEmpty() );

	// create the TestNode
	Node * outputNode = ng.CreateExecNode( outputs, 
										   (FileNode *)inputNode,
										   (FileNode *)compilerNode,
										   arguments,
										   workingDir, 
										   prefixDir,
										   enableRemote,
										   threadLock,
										   expectedReturnCode,
										   retryCount,
										   preBuildDependencies );
	
	return ProcessAlias( funcStartIter, outputNode );
}

// GetCompilerNode
//------------------------------------------------------------------------------
bool FunctionExec::GetCompilerNode(const BFFIterator & iter, const AString & compiler, CompilerNode * & compilerNode) const
{
	NodeGraph & ng = FBuild::Get().GetDependencyGraph();
	Node * cn = ng.FindNode(compiler);
	compilerNode = nullptr;
	if (cn != nullptr)
	{
		if (cn->GetType() == Node::ALIAS_NODE)
		{
			AliasNode * an = cn->CastTo< AliasNode >();
			cn = an->GetAliasedNodes()[0].GetNode();
		}
		if (cn->GetType() != Node::COMPILER_NODE)
		{
			Error::Error_1102_UnexpectedType(iter, this, "Compiler", cn->GetName(), cn->GetType(), Node::COMPILER_NODE);
			return false;
		}
		compilerNode = cn->CastTo< CompilerNode >();
	}
	else
	{
		// create a compiler node - don't allow distribution
		// (only explicitly defined compiler nodes can be distributed)
#ifdef USE_NODE_REFLECTION
		compilerNode = ng.CreateCompilerNode(compiler);
		VERIFY(compilerNode->GetReflectionInfoV()->SetProperty(compilerNode, "AllowDistribution", false));
#else
		const bool allowDistribution = false;
		compilerNode = ng.CreateCompilerNode(compiler, Dependencies(0, false), allowDistribution, AString::GetEmpty());
#endif
	}

	return true;
}

//------------------------------------------------------------------------------
